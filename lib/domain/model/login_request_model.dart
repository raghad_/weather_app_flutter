import 'dart:convert';

class LoginRequest {
  String? email, password;

  LoginRequest({
    this.email,
    this.password,
  });

  String toJson() {
    final data = <String, dynamic>{};
    data['email'] = email;
    data['password'] = password;

    return jsonEncode(data);
  }
}
