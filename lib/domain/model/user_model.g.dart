// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserResponse _$UserResponseFromJson(Map<String, dynamic> json) => UserResponse(
      userName: json['userName'] as String?,
      email: json['email'] as String?,
      uId: json['u_id'] as String?,
      location: json['location'] as String?,
    );

Map<String, dynamic> _$UserResponseToJson(UserResponse instance) =>
    <String, dynamic>{
      'userName': instance.userName,
      'email': instance.email,
      'location': instance.location,
      'u_id': instance.uId,
    };
