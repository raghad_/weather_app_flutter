import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable()
class UserResponse {
  String? userName, email, location;
  @JsonKey(name: "u_id")
  String? uId;

  factory UserResponse.fromJson(Map<String, dynamic> json) =>
      _$UserResponseFromJson(json);

  // to json
  Map<String, dynamic> toJson() => _$UserResponseToJson(this);

  UserResponse(
      {this.userName,
      this.email,
      this.uId,
      this.location});
}
