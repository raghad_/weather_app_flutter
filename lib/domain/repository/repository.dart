import 'package:weather_app/data/network/failure.dart';
import 'package:dartz/dartz.dart';
import '../model/login_request_model.dart';
import '../model/register_model.dart';
import '../model/user_model.dart';

abstract class Repository {
  Future<Either<Failure, void>> login(LoginRequest loginRequest);
  Future<Either<Failure, UserResponse>> register(RegisterRequest registerRequest);
  Future<Either<Failure, UserResponse>> updateUser(UserResponse userResponse);
  Future<Either<Failure, UserResponse>> getUser();
  Future<Either<Failure, void>> signOut();
  Future<Either<Failure, void>> updateEmail(String newEmail,String password);
  Future<Either<Failure, void>> updatePassword(String newPassword);


}
