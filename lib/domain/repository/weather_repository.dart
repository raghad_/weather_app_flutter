import 'package:weather_app/domain/model/weather_model.dart';
import 'package:weather_app/data/network/failure.dart';
import 'package:dartz/dartz.dart';

abstract class WeatherRepository {
  Future<Either<Failure, Weather>> getWeatherByCityName(String cityName);
  Future<Either<Failure, Weather>> getWeatherByLocation(double lon,double lat);

}