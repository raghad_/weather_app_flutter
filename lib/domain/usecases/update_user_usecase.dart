import 'package:dartz/dartz.dart';
import 'package:weather_app/domain/repository/repository.dart';
import '../../data/network/failure.dart';
import '../model/user_model.dart';
import 'base_usecase.dart';

class UpdateUserUseCase implements BaseUseCase<UserResponse, UserResponse> {
  final Repository _repository;

  UpdateUserUseCase(this._repository);

  @override
  Future<Either<Failure, UserResponse>> execute(UserResponse userResponse) async {
    return await _repository.updateUser(userResponse);
  }
}

