import 'package:dartz/dartz.dart';
import '../../data/network/failure.dart';
import '../repository/repository.dart';
import 'base_usecase.dart';

class SignOutUseCase implements BaseUseCase<void, void> {
  final Repository _repository;
  SignOutUseCase(this._repository);

  @override
  Future<Either<Failure, void>> execute(void input) async {
    return await _repository.signOut();
  }
}