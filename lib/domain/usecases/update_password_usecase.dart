import 'package:dartz/dartz.dart';
import 'package:weather_app/domain/repository/repository.dart';
import '../../data/network/failure.dart';
import 'base_usecase.dart';

class UpdatePasswordUseCase implements BaseUseCase<String, void> {
  final Repository _repository;

  UpdatePasswordUseCase(this._repository);

  @override
  Future<Either<Failure, void>> execute(String newPassword) async {
    return await _repository.updatePassword(newPassword);
  }
}

