import 'package:dartz/dartz.dart';
import 'package:weather_app/domain/model/weather_model.dart';
import '../../data/network/failure.dart';
import '../repository/weather_repository.dart';
import 'base_usecase.dart';

class GetWeatherByLocation implements BaseUseCase<PositionRequest, Weather> {
  final WeatherRepository weatherRepository ;

  GetWeatherByLocation(this.weatherRepository);

  @override
  Future<Either<Failure, Weather>> execute(PositionRequest position) async {
    return await weatherRepository.getWeatherByLocation(position.lon, position.lat);
  }
}

class PositionRequest{
  double lon;
  double lat;

  PositionRequest(this.lon, this.lat);
}