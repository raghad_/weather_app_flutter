import 'package:dartz/dartz.dart';
import 'package:weather_app/domain/repository/repository.dart';
import '../../data/network/failure.dart';
import '../model/user_model.dart';
import 'base_usecase.dart';

class GetUserUseCase implements BaseUseCase<void, UserResponse> {
  final Repository _repository;

  GetUserUseCase(this._repository);

  @override
  Future<Either<Failure, UserResponse>> execute(void input) async {
    return await _repository.getUser();
  }
}

