import 'package:dartz/dartz.dart';
import '../../data/network/failure.dart';
import '../model/register_model.dart';
import '../model/user_model.dart';
import '../repository/repository.dart';
import 'base_usecase.dart';

class RegisterUseCase implements BaseUseCase<RegisterRequest, UserResponse> {
  final Repository _repository;

  RegisterUseCase(this._repository);

  @override
  Future<Either<Failure, UserResponse>> execute(RegisterRequest input) async {
    return await _repository.register(input);
  }
}
