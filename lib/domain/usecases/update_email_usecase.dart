import 'package:dartz/dartz.dart';
import 'package:weather_app/domain/repository/repository.dart';
import '../../data/network/failure.dart';
import 'base_usecase.dart';

class UpdateEmailUseCase implements BaseUseCase<UserAuth, void> {
  final Repository _repository;

  UpdateEmailUseCase(this._repository);

  @override
  Future<Either<Failure, void>> execute(UserAuth input) async {
    return await _repository.updateEmail(input.newEmail,input.password);
  }
}
class UserAuth{
  String newEmail;
  String password;

  UserAuth(this.newEmail, this.password);
}

