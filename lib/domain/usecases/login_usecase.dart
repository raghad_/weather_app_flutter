import 'package:dartz/dartz.dart';
import '../../data/network/failure.dart';
import '../model/login_request_model.dart';
import '../repository/repository.dart';
import 'base_usecase.dart';

class LoginUseCase implements BaseUseCase<LoginRequest, void> {
  final Repository _repository;

  LoginUseCase(this._repository);

  @override
  Future<Either<Failure, void>> execute(LoginRequest input) async {
    return await _repository.login(input);
  }
}
