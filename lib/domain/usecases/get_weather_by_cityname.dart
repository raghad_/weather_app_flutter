import 'package:dartz/dartz.dart';
import 'package:weather_app/domain/model/weather_model.dart';
import '../../data/network/failure.dart';
import '../repository/weather_repository.dart';
import 'base_usecase.dart';

class GetWeatherByCityName implements BaseUseCase<String, Weather> {
  final WeatherRepository weatherRepository ;

  GetWeatherByCityName(this.weatherRepository);

  @override
  Future<Either<Failure, Weather>> execute(String cityName) async {
    return await weatherRepository.getWeatherByCityName(cityName);
  }
}
