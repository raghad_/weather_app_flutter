import 'dart:ffi';
import 'package:flutter/material.dart';
import 'package:weather_app/domain/model/user_model.dart';
import 'package:weather_app/domain/usecases/get_user_usecase.dart';
import 'package:weather_app/domain/usecases/update_email_usecase.dart';
import 'package:weather_app/domain/usecases/update_password_usecase.dart';
import 'package:weather_app/domain/usecases/update_user_usecase.dart';
import '../../app/app_prefs.dart';
import '../../app/core/base_model.dart';
import '../../app/di.dart';

class ProfileScreenModel extends BaseModel {
  GetUserUseCase getUserUseCase;
  UpdateUserUseCase updateUserUseCase;
  UpdateEmailUseCase updateEmailUseCase;
  UpdatePasswordUseCase updatePasswordUseCase;

  ProfileScreenModel(this.getUserUseCase, this.updateUserUseCase,
      this.updateEmailUseCase, this.updatePasswordUseCase);

  final snackBar = const SnackBar(
    content: Text('updated successfully'),
  );
  AppPreferences appPreferences = instance<AppPreferences>();
  final formKey = GlobalKey<FormState>();
  FocusNode nameFocusNode = FocusNode();
  FocusNode passwordFocusNode = FocusNode();
  FocusNode cityFocusNode = FocusNode();
  bool obscureText = true;
  UserResponse? user;
  String? newPassword;
  String? newEmail;

  Future initScreen(BuildContext ctx) async {
    setLoading(true);
    context = ctx;
    user = await appPreferences.getUser();
    notifyListeners();
    if (user != null) setLoading(false);
    await getUserInfo();
    setLoading(false);
  }

  void onObscure() {
    obscureText = !obscureText;
    notifyListeners();
  }

  getUserInfo() async {
    if (user?.uId != null) {
      (await getUserUseCase.execute(Void)).fold((failure) {
        // left -> failure
      }, (data) async {
        user = data;
        notifyListeners();
      });
    }
  }

  setNewPassword(String val) {
    newPassword = val;
    notifyListeners();
  }

  setNewEmail(String val) {
    newEmail = val;
    notifyListeners();
  }

  void submitButton() {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      updateUserInfo();
    }
  }

  updateUserInfo() async {
    setLoading(true);
    if (newPassword?.trim() != null && newPassword?.trim() != '') {
      (await updatePasswordUseCase.execute(newPassword!)).fold((failure) {
        // left -> failure
      }, (data) async {
        //password changed
      });
    }
    (await updateUserUseCase.execute(user!)).fold((failure) {
      // left -> failure
    }, (data) async {
      user = data;
      notifyListeners();
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      Navigator.pop(context);
    });
    setLoading(false);
  }
}
