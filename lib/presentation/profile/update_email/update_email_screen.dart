import 'package:flutter/material.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:weather_app/presentation/profile/update_email/update_email_screen_model.dart';
import '../../../app/core/base_widget.dart';
import '../../../app/di.dart';
import '../../auth/local_widgets/email_validate.dart';
import '../../auth/local_widgets/rectangle_button.dart';
import '../../auth/local_widgets/text_field.dart';
import '../../resources/color_manager.dart';
import '../../resources/size_config.dart';

class UpdateEmailScreen extends StatefulWidget {
  const UpdateEmailScreen({Key? key}) : super(key: key);

  @override
  _UpdateEmailScreenState createState() => _UpdateEmailScreenState();
}

class _UpdateEmailScreenState extends State<UpdateEmailScreen> {
  final UpdateEmailScreenModel _viewModel = instance<UpdateEmailScreenModel>();

  @override
  Widget build(BuildContext context) {
    return BaseWidget<UpdateEmailScreenModel>(
        model: _viewModel,
        onModelReady: (model, context) async {
          await model.initScreen(context);
        },
        earlyOnModelReady: true,
        widgetBuilder: (context, model, child) {
          return LoadingOverlay(
            isLoading: model.loading,
            child: Scaffold(
                resizeToAvoidBottomInset: false,
                backgroundColor: Colors.blue.withOpacity(0.6),
                body: Container(
                  height: SizeConfig.screenHeight,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.blue.withOpacity(0.2),
                        Colors.blue.withOpacity(0.4),
                        Colors.blue.withOpacity(0.6),
                        Colors.blue
                      ],
                      begin: Alignment.bottomLeft,
                      end: Alignment.topRight,
                    ),
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: SizeConfig.blockSizeVertical * 8,
                              horizontal: SizeConfig.blockSizeHorizontal * 3),
                          child: Row(
                            children: [
                              IconButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                icon: Icon(Icons.arrow_back_ios),
                                color: Colors.white,
                              ),
                              Text(
                                'Update Email',
                                style: TextStyle(
                                    fontSize:
                                        SizeConfig.blockSizeHorizontal * 8,
                                    color: ColorManager.white,
                                    fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                        ),
                        buildForm(context, model),
                      ],
                    ),
                  ),
                )),
          );
        });
  }

  Widget buildForm(BuildContext context, UpdateEmailScreenModel model) {
    return Padding(
      padding: EdgeInsets.symmetric(
              vertical: SizeConfig.blockSizeVertical,
              horizontal: SizeConfig.blockSizeHorizontal * 5)
          .copyWith(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Form(
          key: model.formKey,
          child: Wrap(
            children: [
              Text('Enter new Email',
                  style: TextStyle(
                      color: Colors.yellowAccent,
                      fontSize: SizeConfig.blockSizeHorizontal * 4)),
              weatherTextField(
                  initValue: model.user?.email,
                  focusNode: model.emailFocusNode,
                  onFieldSubmitted: (term) {
                    model.emailFocusNode.unfocus();
                    FocusScope.of(context)
                        .requestFocus(model.passwordFocusNode);
                  },
                  onSaved: (val) {
                    model.setNewEmail(val!);
                  },
                  hintText: "new Email",
                  validator: (String? fieldContent) {
                    if (fieldContent!.isEmpty) {
                      return 'Field can not be empty';
                    } else if (!validateEmail(fieldContent)) {
                      return 'please enter a valid email';
                    }
                    if (fieldContent == model.user?.email) {
                      return 'Please enter new email';
                    }
                    return null;
                  }),
              Text('Enter your password',
                  style: TextStyle(
                      color: Colors.yellowAccent,
                      fontSize: SizeConfig.blockSizeHorizontal * 4)),
              appPasswordField(
                  focusNode: model.passwordFocusNode,
                  onFieldSubmitted: (term) {
                    model.passwordFocusNode.unfocus();
                  },
                  obscureText: model.obscureText,
                  onPressed: model.onObscure,
                  onSaved: (val) {
                    model.setPassword(val!);
                  },
                  hintText: "Password",
                  validator: (String? fieldContent) {
                    if (fieldContent!.isEmpty) {
                      return 'Field can not be empty';
                    }
                    return null;
                  }),
              buildSubmitButton(model)
            ],
          )),
    );
  }

  Widget buildSubmitButton(UpdateEmailScreenModel model) {
    return appColoredButton(model.context,
        margin: EdgeInsets.symmetric(
          horizontal: SizeConfig.blockSizeHorizontal * 10,
        ).copyWith(top: SizeConfig.blockSizeVertical * 2),
        text: 'Update Email', onPressed: () {
      model.submitButton();
    });
  }
}
