import 'package:flutter/material.dart';
import 'package:weather_app/domain/model/user_model.dart';
import 'package:weather_app/domain/usecases/update_email_usecase.dart';
import '../../../app/app_prefs.dart';
import '../../../app/core/base_model.dart';
import '../../../app/di.dart';

class UpdateEmailScreenModel extends BaseModel {
  UpdateEmailUseCase updateEmailUseCase;

  UpdateEmailScreenModel(this.updateEmailUseCase);

  AppPreferences appPreferences = instance<AppPreferences>();
  final formKey = GlobalKey<FormState>();
  FocusNode emailFocusNode = FocusNode();
  FocusNode passwordFocusNode = FocusNode();
  bool obscureText = true;
  UserResponse? user;
  String? password;
  String? newEmail;

  Future initScreen(BuildContext ctx) async {
    setLoading(true);
    context = ctx;
    user = await appPreferences.getUser();
    notifyListeners();
    setLoading(false);
  }

  void onObscure() {
    obscureText = !obscureText;
    notifyListeners();
  }

  setPassword(String val) {
    password = val;
    notifyListeners();
  }

  setNewEmail(String val) {
    newEmail = val;
    notifyListeners();
  }

  void submitButton() {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      updateEmail();
    }
  }

  updateEmail() async {
    setLoading(true);
    if ((newEmail != null || newEmail?.trim() != '') &&
        newEmail != user?.email) {
      (await updateEmailUseCase.execute(UserAuth(newEmail!, password!))).fold(
          (failure) {
            showErrorDialog(failure.message);
        // left -> failure
      }, (data) async {
        user?.email = newEmail;
        notifyListeners();
        Navigator.pop(context);
      });
    }
    setLoading(false);
  }
}
