import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:weather_app/presentation/profile/profile_screen_model.dart';
import '../../app/core/base_widget.dart';
import '../../app/di.dart';
import '../auth/local_widgets/rectangle_button.dart';
import '../auth/local_widgets/text_field.dart';
import '../resources/color_manager.dart';
import '../resources/routes_manager.dart';
import '../resources/size_config.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final ProfileScreenModel _viewModel = instance<ProfileScreenModel>();

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ProfileScreenModel>(
        model: _viewModel,
        onModelReady: (model, context) async {
          await model.initScreen(context);
        },
        earlyOnModelReady: true,
        widgetBuilder: (context, model, child) {
          return LoadingOverlay(
            isLoading: model.loading,
            child: Scaffold(
                resizeToAvoidBottomInset: false,
                backgroundColor: Colors.blue.withOpacity(0.6),
                body: Container(
                  height: SizeConfig.screenHeight,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.blue.withOpacity(0.2),
                        Colors.blue.withOpacity(0.4),
                        Colors.blue.withOpacity(0.6),
                        Colors.blue
                      ],
                      begin: Alignment.bottomLeft,
                      end: Alignment.topRight,
                    ),
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: SizeConfig.blockSizeVertical * 8,
                              horizontal: SizeConfig.blockSizeHorizontal * 3),
                          child: Row(
                            children: [
                              IconButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                icon: Icon(Icons.arrow_back_ios),
                                color: Colors.white,
                              ),
                              Text(
                                'Update profile',
                                style: TextStyle(
                                    fontSize:
                                        SizeConfig.blockSizeHorizontal * 8,
                                    color: ColorManager.white,
                                    fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                        ),
                        buildForm(context, model),
                      ],
                    ),
                  ),
                )),
          );
        });
  }

  Widget buildForm(BuildContext context, ProfileScreenModel model) {
    return Padding(
      padding: EdgeInsets.symmetric(
              vertical: SizeConfig.blockSizeVertical,
              horizontal: SizeConfig.blockSizeHorizontal * 5)
          .copyWith(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Form(
          key: model.formKey,
          child: Wrap(
            children: [
              weatherTextField(
                  initValue: model.user!.userName,
                  focusNode: model.nameFocusNode,
                  onFieldSubmitted: (term) {
                    model.nameFocusNode.unfocus();
                    FocusScope.of(context).requestFocus(model.cityFocusNode);
                  },
                  onSaved: (val) {
                    model.user?.userName = val;
                  },
                  hintText: "Username",
                  validator: (String? fieldContent) {
                    if (fieldContent!.isEmpty) {
                      return 'could not be empty';
                    }
                    return null;
                  }),
              SizedBox(
                height: SizeConfig.blockSizeVertical * 10,
              ),
              appPasswordField(
                  focusNode: model.passwordFocusNode,
                  onFieldSubmitted: (term) {
                    model.passwordFocusNode.unfocus();
                  },
                  obscureText: model.obscureText,
                  onPressed: model.onObscure,
                  onSaved: (val) {
                    model.setNewPassword(val!);
                  },
                  hintText: "New Password",
                  validator: (String? fieldContent) {
                    if (fieldContent!.isNotEmpty) {
                      if (fieldContent.length < 6) {
                        return 'weak password';
                      }
                    }
                    return null;
                  }),
              SizedBox(
                height: SizeConfig.blockSizeVertical * 10,
              ),
              weatherTextField(
                  initValue: model.user?.location ,
                  focusNode: model.cityFocusNode,
                  onFieldSubmitted: (term) {
                    model.cityFocusNode.unfocus();
                    FocusScope.of(context).requestFocus(model.cityFocusNode);
                  },
                  onSaved: (val) {
                    model.user?.location = val;
                  },
                  hintText: "City name",
                  validator: (String? fieldContent) {
                    if (fieldContent!.isEmpty) {
                      return 'could not be empty';
                    }
                    return null;
                  }),
              InkWell(
                onTap: () {
                  Navigator.pushNamed(context, Routes.updateEmail)
                      .then((value) => model.initScreen(context));
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '  Change your Email',
                      style: TextStyle(
                          color: Colors.yellowAccent,
                          fontSize: SizeConfig.blockSizeHorizontal * 4),
                    ),
                    IconButton(
                      onPressed: () {
                        Navigator.pushNamed(context, Routes.updateEmail)
                            .then((value) => model.initScreen(context));
                      },
                      icon: const Icon(Icons.arrow_forward_ios),
                      iconSize: 18,
                      color: Colors.yellowAccent,
                    )
                  ],
                ),
              ),
              buildSubmitButton(model)
            ],
          )),
    );
  }

  Widget buildSubmitButton(ProfileScreenModel model) {
    return appColoredButton(model.context,
        margin: EdgeInsets.symmetric(
          horizontal: SizeConfig.blockSizeHorizontal * 10,
        ).copyWith(top: SizeConfig.blockSizeVertical * 2),
        text: 'Save', onPressed: () {
      model.submitButton();
    });
  }
}
