import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:weather_app/presentation/resources/color_manager.dart';
import 'package:weather_app/presentation/resources/size_config.dart';

class ClickableText extends StatefulWidget {
  String? text;
  String? clickableText;
  Function? onTap;
  TextStyle? textStyle;
  EdgeInsetsGeometry? padding;

  ClickableText(
      {Key? key, this.text,
      this.textStyle,
        this.clickableText,
      this.onTap,
      this.padding}) : super(key: key);

  @override
  State<ClickableText> createState() => _ClickableTextState();
}

class _ClickableTextState extends State<ClickableText> {
  buildText(BuildContext context) {
    return Padding(
      padding: widget.padding == null ? EdgeInsets.only(top: SizeConfig.blockSizeVertical*15) : widget.padding!,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          RichText(
            text: TextSpan(
                text: widget.text,
                style: widget.textStyle ?? Theme.of(context).textTheme.bodyText1!.copyWith(color: ColorManager.white),)
          ),
          SizedBox(width: 2,),
          RichText(
            text: TextSpan(
                text: widget.clickableText,
                style: widget.textStyle ?? Theme.of(context).textTheme.bodyText1!
                    .copyWith(color: Colors.yellowAccent,fontWeight: FontWeight.bold,
                fontSize: SizeConfig.blockSizeHorizontal*3.5),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    widget.onTap!();
                  }),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return buildText(context);
  }
}
