import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:weather_app/presentation/resources/color_manager.dart';
import '../../resources/size_config.dart';

class RoundedButton extends StatelessWidget {
  final String? text;
  final Function? onPressed;
  final EdgeInsetsGeometry? padding;
  final EdgeInsetsGeometry? margin;
  final TextStyle? textStyle;
  final Color? color;
  final Color? borderColor;
  final double? borderWidth;
  const RoundedButton(
      {this.text,
      this.padding,
      this.margin,
      this.color,
      this.onPressed,
      this.textStyle,
      this.borderColor,
      this.borderWidth});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: margin,
        child: MaterialButton(
          padding: padding,
          color: color,
          elevation: 0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
              side: borderColor != null
                  ? BorderSide(color: borderColor!, width: borderWidth ?? 1)
                  : BorderSide.none),
          onPressed: () {
            onPressed!();
          },
          child: Text(text!,
              style: textStyle ?? Theme.of(context).textTheme.bodyText1),
        ));
  }
}

Widget appColoredButton(BuildContext? context,
    {Function? onPressed,
    required String text,
    EdgeInsetsGeometry? padding,
    EdgeInsetsGeometry? margin,
    Color? color}) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Expanded(
        child: RoundedButton(
          margin: margin ??
              EdgeInsets.symmetric(vertical: SizeConfig.blockSizeVertical * 2),
          padding: padding ??
              EdgeInsets.symmetric(
                vertical: SizeConfig.blockSizeVertical * 2.2,
              ),
          text: tr(text),
          textStyle: TextStyle(color: ColorManager.white,fontSize: SizeConfig.blockSizeHorizontal*5),
          onPressed: onPressed,
          color: color ?? Theme.of(context!).accentColor,
        ),
      ),
    ],
  );
}

Widget appUnColoredButton(BuildContext context,
    {Function? onPressed, required String text, EdgeInsetsGeometry? padding}) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Expanded(
        child: RoundedButton(
          margin:
              EdgeInsets.symmetric(vertical: SizeConfig.blockSizeVertical * 2),
          padding: padding ??
              EdgeInsets.symmetric(
                vertical: SizeConfig.blockSizeVertical * 2,
              ),
          text: tr(text),
          onPressed: onPressed,
          borderColor: Theme.of(context).accentColor,
        ),
      )
    ],
  );
}

class RectangleButton extends StatelessWidget {
  String? text;
  Function? onPressed;
  EdgeInsetsGeometry? padding;
  EdgeInsetsGeometry? margin;
  TextStyle? textStyle;
  Color? color;
  //edit roundness
  double roundness;
  Color? borderColor;
  RectangleButton(
      {this.text,
      this.onPressed,
      this.padding,
      this.margin,
      this.color,
      this.textStyle,
      //edit roundness and border color
      this.roundness = 12,
      this.borderColor});
  buildButton(BuildContext context) {
    // final textStyle = Theme.of(context).textTheme;

    return Container(
        margin: margin,
        child: MaterialButton(
          padding: padding,
          color: color ?? Theme.of(context).accentColor,
          elevation: 0,
          disabledColor: Theme.of(context).dividerColor,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(roundness),
              //side: BorderSide.none),
              side: borderColor == null
                  ? BorderSide.none
                  : BorderSide(color: borderColor!)),
          //side: BorderSide(color: borderColor)),
          onPressed: onPressed as void Function()?,
          child: Text(text!,
              style: textStyle ?? Theme.of(context).textTheme.bodyText1),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return buildButton(context);
  }
}
