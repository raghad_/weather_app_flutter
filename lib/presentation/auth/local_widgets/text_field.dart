import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'dart:ui' as ui;
import '../../resources/color_manager.dart';
import '../../resources/size_config.dart';
import '../../resources/values_manager.dart';

Widget weatherTextField({
  double? width,
  double? height,
  String? initValue,
  TextEditingController? controller,
  Function(String)? onChanged,
  FormFieldSetter<String>? onSaved,
  ui.TextDirection? direction,
  String? Function(String?)? validator,
  ValueChanged<String>? onFieldSubmitted,
  double? borderSize,
  Widget? suffixIcon,
  VoidCallback? onEditingComplete,
  String? hintText,
  FocusNode? focusNode,
  TextInputAction? textInputAction,
}) {
  return Center(
    child: TextFormField(
      textInputAction: textInputAction ?? TextInputAction.next,
      focusNode: focusNode,
      validator: validator,
      onFieldSubmitted: onFieldSubmitted,
      onChanged: onChanged,
      controller: controller,
      textDirection: direction ?? ui.TextDirection.ltr,
      initialValue: initValue,
      textAlignVertical: TextAlignVertical.center,
      style: TextStyle(
          fontSize: SizeConfig.blockSizeHorizontal * 3.8,
          fontFamily: 'tajawal',
          fontWeight: FontWeight.w600,
          color: ColorManager.black),
      onSaved: onSaved,
      onEditingComplete: onEditingComplete,
      decoration: InputDecoration(
        suffixIcon: IconButton(
          onPressed: (){},
          icon: const Icon(Icons.check),
          color: Colors.transparent,
        ),
        // isCollapsed: true,
        errorStyle: const TextStyle(fontSize: 12,color: Colors.redAccent),
        hintText: hintText?.tr(),
        fillColor: Colors.white,
        contentPadding: EdgeInsets.symmetric(
            horizontal: SizeConfig.blockSizeHorizontal * 4,),
        focusColor: Colors.white,
        filled: true,
        hintStyle: TextStyle(
            fontSize: SizeConfig.blockSizeHorizontal * 3.8,
            fontFamily: 'tajawal',
            // height: 1.2,
            fontWeight: FontWeight.w600,
            color: ColorManager.hintText),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(borderSize ?? AppSize.s14),
          borderSide:
              BorderSide(width: 1.5, color: ColorManager.unSelectedColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(borderSize ?? AppSize.s14),
          borderSide: BorderSide(width: 1.5, color: ColorManager.primary),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(borderSize ?? AppSize.s14),
          borderSide:
              BorderSide(width: 1.5, color: ColorManager.unSelectedColor),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(borderSize ?? AppSize.s14),
          borderSide: BorderSide(width: 1.5, color: ColorManager.redColor),
        ),
      ).copyWith(isDense: true),
    ),
  );
}

Widget appPasswordField(
    {double? width,
    double? height,
    String? initValue,
    TextEditingController? controller,
    Function(String)? onChanged,
    FormFieldSetter<String>? onSaved,
    ui.TextDirection? direction,
    ValueChanged<bool>? onFocusChange,
    @required FocusNode? focusNode,
    String? Function(String?)? validator,
    ValueChanged<String>? onFieldSubmitted,
    double? borderSize,
    Widget? suffixIcon,
    bool? obscureText = false,
    VoidCallback? onEditingComplete,
    void Function()? onPressed,
    String? hintText}) {
  return  Focus(
      onFocusChange: onFocusChange,
      child: Center(
        child: TextFormField(
          validator: validator,
          onFieldSubmitted: onFieldSubmitted,
          onChanged: onChanged,
          controller: controller,
          textDirection: direction ?? ui.TextDirection.ltr,
          initialValue: initValue,
          textAlignVertical: TextAlignVertical.center,
          style: TextStyle(
              fontSize: SizeConfig.blockSizeHorizontal * 3.8,
              // height: 1.2,
              fontFamily: 'tajawal',
              fontWeight: FontWeight.w600,
              color: ColorManager.black),
          onSaved: onSaved,
          onEditingComplete: onEditingComplete,
          focusNode: focusNode,
          obscureText: obscureText ?? false,
          decoration: InputDecoration(
            // counterText: '  ',
            suffixIcon: IconButton(
              onPressed: onPressed,
              icon: Icon(
                obscureText! ? Icons.visibility_off : Icons.visibility,
              ),
            ),
            isCollapsed: true,
            errorStyle: const TextStyle(fontSize: 12,color: Colors.redAccent),
            hintText: hintText?.tr(),
            fillColor: Colors.white,
            contentPadding: EdgeInsets.symmetric(
              horizontal: SizeConfig.blockSizeHorizontal * 4,
            ),
            focusColor: Colors.white,
            filled: true,
            hintStyle: TextStyle(
                fontSize: SizeConfig.blockSizeHorizontal * 3.8,
                fontFamily: 'tajawal',
                // height: 1.2,
                fontWeight: FontWeight.w600,
                color: ColorManager.hintText),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(borderSize ?? AppSize.s14),
              borderSide:
                  BorderSide(width: 1.5, color: ColorManager.unSelectedColor),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(borderSize ?? AppSize.s14),
              borderSide: BorderSide(width: 1.5, color: ColorManager.primary),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(borderSize ?? AppSize.s14),
              borderSide:
                  BorderSide(width: 1.5, color: ColorManager.unSelectedColor),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(borderSize ?? AppSize.s14),
              borderSide: BorderSide(width: 1.5, color: ColorManager.redColor),
            ),
          ).copyWith(isDense: true),
        ),
      ),
    );
}
