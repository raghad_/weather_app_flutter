import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:weather_app/presentation/auth/local_widgets/text_field.dart';
import 'package:weather_app/presentation/resources/color_manager.dart';
import '../../../app/core/base_widget.dart';
import '../../../app/di.dart';
import '../../resources/routes_manager.dart';
import '../../resources/size_config.dart';
import '../local_widgets/clickable_Text.dart';
import '../local_widgets/email_validate.dart';
import '../local_widgets/rectangle_button.dart';
import 'login_screen_model.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final LoginScreenModel _viewModel = instance<LoginScreenModel>();
  @override
  Widget build(BuildContext context) {
    return BaseWidget<LoginScreenModel>(
        model: _viewModel,
        onModelReady: (model, context) {
          model.initScreen(context);
        },
        earlyOnModelReady: true,
        onModelDestroy: (model) {
          model.passwordFocusNode.dispose();
          model.emailFocusNode.dispose();
        },
        widgetBuilder: (context, model, child) {
          return LoadingOverlay(
            isLoading: model.loading,
            child: GestureDetector(
              onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
              child: Scaffold(
                  resizeToAvoidBottomInset: false,
                  backgroundColor: Colors.blue.withOpacity(0.6),
                  body: Container(
                    height: SizeConfig.screenHeight,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Colors.blue.withOpacity(0.4),
                          Colors.blue.withOpacity(0.5),
                          Colors.blue.withOpacity(0.9),
                          Colors.blue
                        ],
                        begin: Alignment.bottomLeft,
                        end: Alignment.topRight,
                      ),
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: SizeConfig.blockSizeVertical * 8,
                                horizontal: SizeConfig.blockSizeHorizontal * 3),
                            child: Text(
                              "Login",
                              style: TextStyle(
                                  fontSize: SizeConfig.blockSizeHorizontal * 8,
                                  color: ColorManager.white,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                          Center(
                              child: Image.asset(
                            "assets/images/cloud.png",
                            height: SizeConfig.screenHeight * 0.3,
                            width: SizeConfig.screenWidth * 0.5,
                          )),
                          buildTipText(context),
                          buildForm(context, model),
                          Center(
                            child: ClickableText(
                              text: "don't have an account?",
                              clickableText: "Signup",
                              onTap: () {
                                initRegisterModule();
                                Navigator.pushReplacementNamed(
                                    context, Routes.registerRoute);
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  )),
            ),
          );
        });
  }

  Widget buildTipText(BuildContext context) {
    return Center(
      child: Text(tr('Enter your login credentials to access your account'),
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodyText2!.copyWith(
              color: Colors.yellowAccent, fontWeight: FontWeight.w500)),
    );
  }

  Widget buildForm(BuildContext context, LoginScreenModel model) {
    return Padding(
      padding: EdgeInsets.symmetric(
              vertical: SizeConfig.blockSizeVertical,
              horizontal: SizeConfig.blockSizeHorizontal * 5)
          .copyWith(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Form(
          key: model.formKey,
          child: Wrap(
            children: [
              weatherTextField(
                  focusNode: model.emailFocusNode,
                  onFieldSubmitted: (term) {
                    model.emailFocusNode.unfocus();
                    FocusScope.of(context)
                        .requestFocus(model.passwordFocusNode);
                  },
                  onSaved: (val) {
                    model.requestData.email = val;
                  },
                  hintText: "Email",
                  validator: (String? fieldContent) {
                    if (fieldContent!.isEmpty ) {
                      return 'could not be empty !';
                    }
                    else if( !validateEmail(fieldContent))
                    {
                      return 'please enter a valid email';
                    }
                    return null;
                  }),
              SizedBox(
                height: SizeConfig.blockSizeVertical * 10,
              ),
              appPasswordField(
                  focusNode: model.passwordFocusNode,
                  onFieldSubmitted: (term) {
                    model.passwordFocusNode.unfocus();
                  },
                  obscureText: model.obscureText,
                  onPressed: model.onObscure,
                  onSaved: (val) {
                    model.requestData.password = val;
                  },
                  hintText: "Password",
                  validator: (String? fieldContent) {
                    if (fieldContent!.isEmpty) {
                      return 'could not be empty !';
                    }
                    return null;
                  }),
              buildSubmitButton(model)
            ],
          )),
    );
  }

  Widget buildSubmitButton(LoginScreenModel model) {
    return appColoredButton(model.context,
        margin: EdgeInsets.symmetric(
          horizontal: SizeConfig.blockSizeHorizontal * 10,
        ).copyWith(top: SizeConfig.blockSizeVertical * 2),
        text: 'Login', onPressed: () {
      model.submitButton();
    });
  }
}
