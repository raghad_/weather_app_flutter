import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:weather_app/domain/usecases/login_usecase.dart';
import '../../../app/app_prefs.dart';
import '../../../app/core/base_model.dart';
import '../../../app/di.dart';
import '../../../domain/model/login_request_model.dart';
import '../../resources/routes_manager.dart';
import 'package:easy_localization/easy_localization.dart';

enum PermissionGroup {
  locationAlways,
  locationWhenInUse
}

class LoginScreenModel extends BaseModel {
  AppPreferences appPreferences = instance<AppPreferences>();
  LoginUseCase loginUseCase;

  LoginScreenModel(
    this.loginUseCase,
  );

  final formKey = GlobalKey<FormState>();
  LoginRequest requestData = LoginRequest();
  FocusNode emailFocusNode = FocusNode();
  FocusNode passwordFocusNode = FocusNode();
  bool obscureText = true;

  void initScreen(BuildContext ctx) async {
    context = ctx;

    final requested = await [
      Permission.locationWhenInUse,
      Permission.locationAlways
    ].request();

    final alwaysGranted =
        requested[PermissionGroup.locationAlways] == PermissionStatus.granted;
    final whenInUseGranted = requested[PermissionGroup.locationWhenInUse] ==
        PermissionStatus.granted;
  }

  void submitButton() {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      login(requestData);
    }
  }

  login(LoginRequest requestData) async {
    setLoading(true);
    (await loginUseCase.execute(requestData)).fold((failure) async {
      setLoading(false);
      showErrorDialog(failure.message.tr());
    }, (data) {
      Navigator.pushReplacementNamed(context, Routes.mainRoute);
    });
    setLoading(false);
  }

  void onObscure() {
    obscureText = !obscureText;
    notifyListeners();
  }
}
