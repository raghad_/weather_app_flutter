import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../app/app_prefs.dart';
import '../../../app/core/base_model.dart';
import '../../../app/di.dart';
import '../../../domain/model/register_model.dart';
import '../../../domain/usecases/register_usecase.dart';
import '../../resources/routes_manager.dart';

class RegisterScreenModel extends BaseModel {
  AppPreferences appPreferences = instance<AppPreferences>();
  RegisterUseCase registerUseCase;

  RegisterScreenModel(
    this.registerUseCase,
  );

  final formKey = GlobalKey<FormState>();
  RegisterRequest requestData = RegisterRequest();
  FocusNode nameFocusNode = FocusNode();
  FocusNode emailFocusNode = FocusNode();
  FocusNode passwordFocusNode = FocusNode();

  bool obscureText = true;

  void initScreen(BuildContext ctx) {
    context = ctx;
  }

  void submitButton() {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      register(requestData);
    }
  }

  register(RegisterRequest requestData) async {
    setLoading(true);
    (await registerUseCase.execute(requestData)).fold((failure) async {
      setLoading(false);
      showErrorDialog(failure.message.tr());
    }, (data) {
      setLoading(false);
      Navigator.pushReplacementNamed(context, Routes.mainRoute);
    });
  }

  void onObscure() {
    obscureText = !obscureText;
    notifyListeners();
  }
}
