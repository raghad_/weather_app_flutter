import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:weather_app/domain/model/user_model.dart';
import 'package:weather_app/domain/usecases/get_user_usecase.dart';
import 'package:weather_app/domain/usecases/get_weather_by_cityname.dart';
import '../../../app/app_prefs.dart';
import '../../../app/core/base_model.dart';
import '../../../app/di.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoding/geocoding.dart';
import '../../domain/model/weather_model.dart';
import '../../domain/usecases/get_weather_by_location.dart';
import '../../domain/usecases/signout_usecase.dart';
import '../resources/routes_manager.dart';

class MainScreenModel extends BaseModel {
  GetWeatherByCityName getWeatherByCityName;
  GetWeatherByLocation getWeatherByLocation;
  GetUserUseCase getUserUseCase;
  SignOutUseCase signOutUseCase;

  MainScreenModel(this.getWeatherByLocation, this.getWeatherByCityName,
      this.signOutUseCase, this.getUserUseCase);

  AppPreferences appPreferences = instance<AppPreferences>();
  Position? position;
  Weather? weather;
  UserResponse? user;
  bool? notFound=false;

  void initScreen(BuildContext ctx) async {
    setLoading(true);
    weather=null;
    country= null;
    notFound=false;
    context = ctx;
    user = await appPreferences.getUser();
    if (user != null) {
      if (user?.location == null || user?.location == '') {
        await getCurrentLocation();
        if (position?.latitude != null && position?.longitude != null) {
          getWeatherByPosition();
        }
      } else {
        getWeatherByCity(user!.location!);
        country =user!.location!;
      }
    } else {
      await getUserInfo();
      if (user?.location == null || user?.location == '') {
        await getCurrentLocation();
        if (position?.latitude != null && position?.longitude != null) {
          getWeatherByPosition();
        }
      } else {
        getWeatherByCity(user!.location!);
        country =user!.location!;

      }
    }
    notifyListeners();

    setLoading(false);
  }

  String? country = "";

  Future getPlace() async {
    List<Placemark> newPlace =
        await placemarkFromCoordinates(position!.latitude, position!.longitude);
    Placemark placeMark = newPlace[0];
    country = placeMark.country;
    notifyListeners();
  }

  Future getCurrentLocation() async {
    var status = await Permission.locationWhenInUse.status;
    if (status == PermissionStatus.granted) {
      position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.low);
      if (position != null) {
        await getPlace();
      }
    }else{
      notFound =true;
      notifyListeners();
    }

    notifyListeners();
  }

  getWeatherByPosition() async {
    setLoading(true);
    (await getWeatherByLocation
            .execute(PositionRequest(position!.longitude, position!.latitude)))
        .fold((failure) {
      setLoading(false);
      notFound=true;
    }, (data) async {
      setLoading(false);
      weather = data;
      print(weather?.main.temp);
      notifyListeners();
    });
  }

  getWeatherByCity(String city) async {
    weather=null;
    country=null;
    setLoading(true);
    (await getWeatherByCityName.execute(city.trim())).fold((failure) {
      // left -> failure
      notFound=true;
      notifyListeners();
      setLoading(false);
      showErrorDialog('not Found');
    }, (data) async {
      setLoading(false);
      weather = data;
      country =city;
      notifyListeners();
    });
  }

  getUserInfo() async {
    if (user?.uId != null) {
      (await getUserUseCase.execute(Void)).fold((failure) {
        // left -> failure
      }, (data) async {
        user = data;
        notifyListeners();
      });
    }
  }

  signOut() async {
    (await signOutUseCase.execute(Void)).fold((failure) {
      // left -> failure
    }, (data) async {
      appPreferences.logout();
      Navigator.pushReplacementNamed(context, Routes.loginRoute);
    });
  }


}
