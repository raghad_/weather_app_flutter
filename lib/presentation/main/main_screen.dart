import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:weather_app/presentation/resources/color_manager.dart';
import '../../../app/core/base_widget.dart';
import '../../../app/di.dart';
import '../resources/routes_manager.dart';
import '../resources/size_config.dart';
import 'local_widgets/search_field.dart';
import 'main_screen_model.dart';
import 'package:intl/intl.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final MainScreenModel _viewModel = instance<MainScreenModel>();
  final searchFieldController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BaseWidget<MainScreenModel>(
        model: _viewModel,
        onModelReady: (model, context) {
          model.initScreen(context);
        },
        earlyOnModelReady: true,
        widgetBuilder: (context, model, child) {
          return LoadingOverlay(
            isLoading: model.loading,
            child: Scaffold(
                resizeToAvoidBottomInset: false,
                backgroundColor: Colors.blue.withOpacity(0.6),
                appBar: AppBar(
                  centerTitle: true,
                  title: const Text('Home'),
                  leading: IconButton(
                    icon: const Icon(Icons.person),
                    color: Colors.white,
                    tooltip: 'Profile',
                    onPressed: () {
                      searchFieldController.text = '';
                      Navigator.pushNamed(context, Routes.updateProfile)
                          .then((value) => model.initScreen(context));
                    },
                  ),
                  actions: [
                    IconButton(
                      onPressed: () {
                        model.signOut();
                      },
                      icon: const Icon(Icons.logout),
                      tooltip: 'Logout',
                    )
                  ],
                ),
                body: Container(
                  height: SizeConfig.screenHeight,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.blue.withOpacity(0.2),
                        Colors.blue.withOpacity(0.4),
                        Colors.blue.withOpacity(0.6),
                        Colors.blue
                      ],
                      begin: Alignment.bottomLeft,
                      end: Alignment.topRight,
                    ),
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: SizeConfig.blockSizeHorizontal * 3,
                              vertical: SizeConfig.blockSizeVertical * 2),
                          child: searchField(
                              searchController: searchFieldController,
                              onSubmitted: (value) {
                                model.getWeatherByCity(value);
                              }),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: SizeConfig.blockSizeVertical * 8,
                              horizontal: SizeConfig.blockSizeHorizontal * 3),
                          child: model.country != null
                              ? Column(
                                  children: [
                                    Text(
                                      model.country ?? '',
                                      style: TextStyle(
                                          fontSize:
                                              SizeConfig.blockSizeHorizontal *
                                                  8,
                                          color: ColorManager.white,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Text(
                                      '${DateFormat('MMM-dd').format(DateTime.now())} Today',
                                      style: const TextStyle(
                                          color: Colors.yellowAccent),
                                    ),
                                  ],
                                )
                              : const SizedBox(),
                        ),
                        model.weather != null
                            ? Column(
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        model.weather?.main.temp.toString() ??
                                            'not Found',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize:
                                                SizeConfig.blockSizeHorizontal *
                                                    15),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            '°C',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: SizeConfig
                                                        .blockSizeHorizontal *
                                                    5),
                                          ),
                                          Text(
                                            model.weather?.weather.first.main ??
                                                '',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: SizeConfig
                                                        .blockSizeHorizontal *
                                                    5),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  Center(
                                      child: Image.asset(
                                    "assets/images/logo.png",
                                    height: SizeConfig.screenHeight * 0.3,
                                    width: SizeConfig.screenWidth * 0.5,
                                  )),
                                ],
                              )
                            : model.notFound!
                                ? Text('Not Found',
                                    style: TextStyle(
                                        fontSize:
                                            SizeConfig.blockSizeHorizontal * 8,
                                        color: ColorManager.white,
                                        fontWeight: FontWeight.w500))
                                : SizedBox(
                                    height: SizeConfig.screenHeight * 0.3,
                                    child: const LoadingIndicator(
                                      indicatorType: Indicator.ballBeat,
                                    ),
                                  ),
                      ],
                    ),
                  ),
                )),
          );
        });
  }
}
