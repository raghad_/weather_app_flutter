import 'package:flutter/material.dart';
import '../../resources/color_manager.dart';
import '../../resources/size_config.dart';

Widget searchField(
    {TextEditingController? searchController,
    Color? textFieldColor,
    EdgeInsets? padding,
    ValueChanged<String>? onChanged,
    void Function(String)? onSubmitted}) {
  return TextField(
    onChanged: onChanged,
    onSubmitted: onSubmitted ??(value){},
    controller: searchController,
      textInputAction: TextInputAction.search,
    decoration: InputDecoration(
      isDense: true,
      hintText: 'Search City',
      contentPadding: padding ??
          EdgeInsets.symmetric(
              vertical: SizeConfig.blockSizeVertical * 2,
              horizontal: SizeConfig.blockSizeHorizontal * 2),
      border: OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadius.circular(10.0),
      ),
      filled: true,
      suffixIconConstraints: const BoxConstraints(
        minHeight: 25,
        minWidth: 25,
      ),
      suffixIcon: Icon(
        Icons.search,
        color: Colors.black,
        size: SizeConfig.blockSizeHorizontal * 4.5,
      ),

      fillColor: textFieldColor ?? ColorManager.white,
      hintStyle: TextStyle(
          color: ColorManager.hintText,
          fontSize: SizeConfig.blockSizeHorizontal * 4.1,
          height: 0.9),
    ),
  );
}
