import 'package:flutter/material.dart';

class ColorManager {
  static final Map<int, Color> primaryColor = {
    50: Color(0xFF4D0598),
    200: Color(0xFF4D0598),
    300: Color(0xFF4D0598),
    400: Color(0xFF4D0598),
    500: Color(0xFF4D0598),
    600: Color(0xFF4D0598),
    700: Color(0xFF4D0598),
    800: Color(0xFF4D0598),
    900: Color(0xFF4D0598),
  };
  static Color primary = Colors.blue.withOpacity(0.6);
  static Color secondary = HexColor.fromHex("#31A2A2");
  static Color backgroundColor = HexColor.fromHex("#F1F4F9");
  static Color titleTextColor = HexColor.fromHex("#2E2E2E");
  static Color mainTextColor = HexColor.fromHex("#3a3e64");
  static Color unSelectedColor = HexColor.fromHex("#C6D5ED");
  static Color selectedColor = HexColor.fromHex("#39377C");
  static Color hintText = HexColor.fromHex("#7F9BB4");
  static Color redColor = HexColor.fromHex("#B00020");
  static Color orangeAlert = HexColor.fromHex("#FF7E0D");
  static Color badgeColorAlert = HexColor.fromHex("#FC5F4A");
  static Color darkGrey = HexColor.fromHex("#525252");
  static Color gray = HexColor.fromHex("#cccccc");
  static Color lightGrey = HexColor.fromHex("#f5f5f5");
  static Color primaryOpacity70 = HexColor.fromHex("#B3ED9728");
  static Color iconColor = HexColor.fromHex('#8aa4ce');
  static Color dividerColor = HexColor.fromHex('#c6d5ed');
  static Color logoColor = HexColor.fromHex('#2c2c6d');
  static Color topDividerColor = HexColor.fromHex('#E6E6F0');
  static Color timeMessageColor = HexColor.fromHex('#BFBEBE');

  // new colors
  static Color darkPrimary = HexColor.fromHex("#4D0598");
  // static Color grey1 = HexColor.fromHex("#707070");
  static Color grey2 = HexColor.fromHex("#797979");
  static Color white = HexColor.fromHex("#FFFFFF");
  static Color error = HexColor.fromHex("#e61f34"); // red color
  static Color black = HexColor.fromHex("#000000");
  static Color grayLight = HexColor.fromHex("#A0A7B9"); //#e0eaeaea
  static Color unread = HexColor.fromHex("##fad1d1d6");
  static Color messageInputBorder = HexColor.fromHex("#CCCCCC");
  static Color messageTextColor = HexColor.fromHex("#707070");
  static Color textFieldChatting = HexColor.fromHex("#EFEFEF");
  static Color hintTextColor2 =HexColor.fromHex("#969696");



// red color
}

extension HexColor on Color {
  static Color fromHex(String hexColorString) {
    hexColorString = hexColorString.replaceAll('#', '');
    if (hexColorString.length == 6) {
      hexColorString = "FF" + hexColorString; // 8 char with opacity 100%
    }
    return Color(int.parse(hexColorString, radix: 16));
  }
}
