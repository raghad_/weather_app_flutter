import 'package:flutter/material.dart';

// this class for responsive sizes depend on media query and screen size
class SizeConfig {
  static late MediaQueryData _mediaQueryData;
  static late double devicePixelRatio;
  static late double screenWidth;
  static late double screenHeight;
  static late double blockSizeHorizontal;
  static late double blockSizeVertical;
  static late double size;
  static bool isSmallTablet = false;
  static bool isLargeTablet = false;
  static bool isTablet = false;
  static void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    isSmallTablet = _mediaQueryData.size.shortestSide >= 600 &&
        _mediaQueryData.size.shortestSide < 750;
    isTablet = _mediaQueryData.size.shortestSide >= 600;
    isLargeTablet = _mediaQueryData.size.shortestSide >= 1024;
    devicePixelRatio = _mediaQueryData.devicePixelRatio;
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    blockSizeHorizontal = screenWidth / 100;
    blockSizeVertical = screenHeight / 100;
    size = blockSizeVertical * blockSizeHorizontal;
    final landscape = screenHeight < screenWidth;

    if (isSmallTablet) {
      blockSizeHorizontal = blockSizeHorizontal / 1.2;
    } else if (isTablet) {
      blockSizeHorizontal = blockSizeHorizontal / 1.4;
    } else if (isLargeTablet) {
      blockSizeHorizontal = blockSizeHorizontal / 2;
    } else {}
    if (landscape) {
      final temp = blockSizeHorizontal;
      blockSizeHorizontal = blockSizeVertical;
      blockSizeVertical = temp;
    }
  }
}
