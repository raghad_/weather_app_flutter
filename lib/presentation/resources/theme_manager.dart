import 'package:flutter/material.dart';
import 'package:weather_app/presentation/resources/styles_manager.dart';
import 'package:weather_app/presentation/resources/values_manager.dart';

import 'color_manager.dart';

ThemeData getApplicationTheme() {
  return ThemeData(
    // main colors of the app
    primaryColor: ColorManager.primary,
    primaryColorDark: ColorManager.darkPrimary,
    disabledColor: ColorManager.gray,
    colorScheme: ColorScheme.fromSwatch(
      backgroundColor: Colors.white,
      primarySwatch:
          MaterialColor(ColorManager.primary.value, ColorManager.primaryColor),
    ),
    // ripple color
    splashColor: ColorManager.unSelectedColor,
    // card view theme
    cardTheme: CardTheme(
        color: ColorManager.white,
        shadowColor: ColorManager.gray,
        elevation: AppSize.s4),
    textTheme: TextTheme(
        headline1: getSemiBoldStyle(
            color: ColorManager.titleTextColor, fontSize: 16),
        subtitle1: getMediumStyle(
            color: ColorManager.titleTextColor, fontSize: 14),
        subtitle2: getMediumStyle(
            color: ColorManager.titleTextColor, fontSize: 14),
        caption: getRegularStyle(color: ColorManager.titleTextColor),
        bodyText1: getRegularStyle(color: ColorManager.titleTextColor)),
  );
}
