const String IMAGE_PATH = "assets/images";
const String JSON_PATH = "assets/json";

class AppStrings {

  ///Error STRINGS
  static const String msg_success = "msg_success";
  static const String msg_no_content = "msg_no_content";
  static const String msg_bad_request = "msg_bad_request";
  static const String msg_forbidden = "msg_forbidden";
  static const String msg_unauthorised = "msg_unauthorised";
  static const String msg_not_found = "msg_not_found";
  static const String msg_server_error = "msg_server_error";
  static const String msg_not_active = "msg_not_active";
  static const String msg_too_many_req = "msg_too_many_req";
  static const String msg_unprocessable_entity = "msg_unprocessable_entity";
  static const String msg_default = "msg_default";
  static const String msg_connection_out = "msg_connection_out";
  static const String msg_cancel = "msg_cancel";
  static const String msg_not_internet = "msg_not_internet";
  static const String msg_cache_error = "msg_cache_error";
  static const String msg_no_favorite_data ="msg_no_favorite_data";
  static const String msg_something_wrong = "msg_something_wrong";
  static const String ok = "ok";
  static const String msg_wrong_password = "Wrong Password";
  static const String msg_email_not_found = "User not Found";
  static const String msg_email_exist = "Email already used";


}
