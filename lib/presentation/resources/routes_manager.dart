import 'package:flutter/material.dart';
import 'package:weather_app/presentation/auth/login/login_screen.dart';
import 'package:weather_app/presentation/auth/register/register_screen.dart';
import 'package:weather_app/presentation/main/main_screen.dart';
import 'package:weather_app/presentation/profile/update_email/update_email_screen.dart';
import '../../app/di.dart';
import '../profile/profile_screen.dart';
import '../splashScreen.dart';

class Routes {
  static const String mainRoute = "/main";
  static const String loginRoute = "/login";
  static const String splashRoute = "/splash";
  static const String registerRoute = "/register";
  static const String updateProfile = "/profile";
  static const String updateEmail= "/updateEmail";


}

class RouteGenerator {
  static Route<dynamic> getRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case Routes.splashRoute:
        return MaterialPageRoute(builder: (_) => const SplashView(),);
      case Routes.loginRoute:
        initLoginModule();
        return MaterialPageRoute(builder: (_) => const LoginScreen(),);
      case Routes.registerRoute:
        initRegisterModule();
        return MaterialPageRoute(builder: (_) => const RegisterScreen(),);
      case Routes.mainRoute:
        initMainModule();
        return MaterialPageRoute(builder: (_) => const MainScreen(),);
      case Routes.updateProfile:
        initProfileModule();
        return MaterialPageRoute(builder: (_) => const ProfileScreen(),);
      case Routes.updateEmail:
        initUpdateEmailModule();
        return MaterialPageRoute(builder: (_) => const UpdateEmailScreen(),);
      default:
        return unDefinedRoute();
    }
  }


  static Route<dynamic> unDefinedRoute() {
    return MaterialPageRoute(
        builder: (_) => Scaffold(
              appBar: AppBar(
                title: const Text('No Route found'),
              ),
              body: const Center(child: Text('No Route found')),
            ));
  }
}
