import 'dart:async';
import 'package:flutter/material.dart';
import 'package:weather_app/presentation/resources/routes_manager.dart';
import 'package:weather_app/presentation/resources/size_config.dart';
import '../app/app_prefs.dart';
import '../app/di.dart';

class SplashView extends StatefulWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> with TickerProviderStateMixin {
  Timer? _timer;
  final AppPreferences _appPreferences = instance<AppPreferences>();
  AnimationController? animation;
  Animation<double>? _fadeInFadeOut;

  _startDelay() {
    _timer = Timer(const Duration(seconds: 2), _goNext);
  }

  _goNext() async {
    _appPreferences.getUser().then((isUserLoggedIn) async {
      {
        if (isUserLoggedIn != null) {
          Navigator.of(context).pushNamedAndRemoveUntil(
              Routes.mainRoute, (Route<dynamic> route) => false);
        } else {
          Navigator.of(context).pushNamedAndRemoveUntil(
              Routes.loginRoute, (Route<dynamic> route) => false);
        }
      }
    });
  }

  @override
  void initState() {
    super.initState();
    animation = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    );
    _fadeInFadeOut = Tween<double>(begin: 0.3, end: 0.8).animate(animation!);

    animation?.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animation?.reverse();
      } else if (status == AnimationStatus.dismissed) {
        animation?.forward();
      }
    });
    animation?.forward();
    _startDelay();
  }

  @override
  void dispose() {
    _timer?.cancel();
    animation?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue.withOpacity(0.6),
      body: Container(
        height: SizeConfig.screenHeight,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.blue.withOpacity(0.4),
              Colors.blue.withOpacity(0.6),
              Colors.blue.withOpacity(0.9),
              Colors.blue
            ],
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FadeTransition(
              opacity: _fadeInFadeOut!,
              child: Center(
                child: Image.asset(
                  "assets/images/logo.png",
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
