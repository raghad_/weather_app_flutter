

import 'package:flutter/material.dart';

import '../auth/local_widgets/rectangle_button.dart';
import '../resources/color_manager.dart';
import '../resources/size_config.dart';
import '../resources/strings_manager.dart';

Widget errorDialog({
  String? text,
  BuildContext? context,
  VoidCallback? onPressed,
  bool? showCancelButton =false,
  String? confirmText,
}) {
  return Dialog(
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0)), //this right here
    child: Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.blockSizeHorizontal * 12,
                vertical: SizeConfig.blockSizeVertical * 3),
            child: Text(
              text!,
              style: TextStyle(color: ColorManager.titleTextColor,
              fontSize: SizeConfig.blockSizeHorizontal*3.5),maxLines: 4,
              textAlign: TextAlign.center,
            ),
          ),
          RoundedButton(
              text: confirmText ??AppStrings.ok,
              textStyle: TextStyle(fontSize: SizeConfig.blockSizeHorizontal*3.5),
              onPressed: onPressed ??
                  () {
                    Navigator.pop(context!);
                  },
          ),
          showCancelButton!?SizedBox(
            height: SizeConfig.blockSizeVertical * 1,
          ):SizedBox(
            height: SizeConfig.blockSizeVertical * 0,
          ),
          SizedBox(
            height: SizeConfig.blockSizeVertical * 3,
          ),
        ],
      ),
    ),
  );
}


