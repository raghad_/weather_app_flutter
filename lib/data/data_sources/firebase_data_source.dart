import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../../domain/model/login_request_model.dart';
import '../../domain/model/register_model.dart';
import '../../domain/model/user_model.dart';


abstract class FirebaseDataSource {
  Future<String?> login(LoginRequest loginRequest);
  Future<User?> register(RegisterRequest registerRequest);
  Future<UserResponse?> getUser(String uId);
  Future<void> saveUser(UserResponse user);
  Future<UserResponse?> updateUser(UserResponse user);
  Future<void> signOut();
  Future<String?> updateEmail(String email,String newEmail,String password);
  Future<void> updatePassword(String newPassword);

}

class FirebaseDataSourceImplementer implements FirebaseDataSource {
  FirebaseAuth firebaseAuth;
  FirebaseFirestore firebaseFirestore;
  FirebaseDataSourceImplementer(this.firebaseAuth, this.firebaseFirestore);

  @override
  Future<String?> login(LoginRequest loginRequest) async {
    try {
      final credential = await firebaseAuth.signInWithEmailAndPassword(
          email: loginRequest.email!, password: loginRequest.password!);
      return credential.user?.uid;
    } on FirebaseAuthException catch (e) {
      rethrow;
    }
  }

  @override
  Future<User?> register(RegisterRequest registerRequest) async {
    try {
      User? user;
      await firebaseAuth
          .createUserWithEmailAndPassword(
              email: registerRequest.email!,
              password: registerRequest.password!)
          .then((value) async {
        user = firebaseAuth.currentUser;
      });
      return user;
    } on FirebaseAuthException catch (e) {
      rethrow;
    }
  }

  @override
  Future<UserResponse?> getUser(String uid) async {
    try {
      var document = await firebaseFirestore.collection('users').doc(uid).get();
      if (document.data() != null) {
        UserResponse user = UserResponse.fromJson(document.data()!);

        return user;
      } else {
        return null;
      }
    } catch (e) {
      debugPrint('adding user problem');
    }
    return null;
  }

  @override
  Future<void> saveUser(
    UserResponse user,
  ) async {
    try {
      await firebaseFirestore
          .collection("users")
          .doc(user.uId)
          .set(user.toJson());
    } catch (e) {
      debugPrint('adding user problem');
    }
  }

  @override
  Future<UserResponse?> updateUser(UserResponse user) async {
    try {
      final userCollection = firebaseFirestore.collection("users");
      await userCollection.doc(user.uId).update(user.toJson());
      var updatedUser = await userCollection.doc(user.uId).get();
      return UserResponse.fromJson(updatedUser.data()!);
    } catch (e) {
      debugPrint('adding user problem');
      return null;
    }
  }

  @override
  Future<void> signOut() async {
    await firebaseAuth.signOut();
  }

  @override
  Future<String?> updateEmail(String email ,String newEmail,String password) async {
    try {
     final userCredential= await firebaseAuth
          .signInWithEmailAndPassword( email: email, password: password);
      await userCredential.user?.updateEmail(newEmail);
      return 'success';
    } on FirebaseAuthException catch (e) {
      rethrow;
    }
  }

  @override
  Future<void> updatePassword(String newPassword) async {
    try {
      final firebaseUser = firebaseAuth.currentUser;
      firebaseUser?.updatePassword(newPassword);
    } on FirebaseAuthException catch (e) {
      rethrow;
    }
  }


}
