import 'package:weather_app/domain/model/weather_model.dart';
import '../network/app_api.dart';

abstract class WeatherDataSource {
  Future<Weather> getWeatherByLocation(double lat,double lon);
  Future<Weather> getWeatherByCityName(String cityName);

}

class WeatherDataSourceImplementer implements WeatherDataSource {
  AppServiceClient _appServiceClient;

  WeatherDataSourceImplementer(this._appServiceClient);

  @override
  Future<Weather> getWeatherByCityName(String cityName) async{
    return await _appServiceClient.getWeatherByCityName(cityName);
  }

  @override
  Future<Weather> getWeatherByLocation(double lat, double lon) async{
    return await _appServiceClient.getWeatherByLocation(lon, lat);
  }


}
