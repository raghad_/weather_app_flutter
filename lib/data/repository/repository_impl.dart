import 'dart:ffi';
import 'package:dartz/dartz.dart';
import '../../app/app_prefs.dart';
import '../../domain/model/login_request_model.dart';
import '../../domain/model/register_model.dart';
import '../../domain/model/user_model.dart';
import '../../domain/repository/repository.dart';
import '../data_sources/local_data_source.dart';
import '../data_sources/firebase_data_source.dart';
import '../network/error_handler.dart';
import '../network/failure.dart';
import '../network/network_info.dart';

class RepositoryImpl extends Repository {
  final FirebaseDataSource _remoteDataSource;
  final NetworkInfo _networkInfo;
  final AppPreferences _appPreferences;
  final LocalDataSource _localDataSource;

  RepositoryImpl(this._remoteDataSource, this._networkInfo,
      this._appPreferences, this._localDataSource);

  @override
  Future<Either<Failure, void>> login(LoginRequest loginRequest) async {
    if (await _networkInfo.isConnected) {
      try {
        final response = await _remoteDataSource.login(loginRequest);
        if (response != null) {
          UserResponse? user = await _remoteDataSource.getUser(response);
          if (user != null) {
            await _appPreferences.saveUser(user);
          }
          return Right('');
        } else {
          return (Left(ErrorHandler.handle(response).failure));
        }
      } catch (error) {
        return (Left(ErrorHandler.handle(error).failure));
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, UserResponse>> register(
      RegisterRequest registerRequest) async {
    if (await _networkInfo.isConnected) {
      try {
        final response = await _remoteDataSource.register(registerRequest);
        if (response != null) {
          UserResponse user = UserResponse(
              uId: response.uid,
              email: registerRequest.email,
              userName: registerRequest.userName);
          await _remoteDataSource.saveUser(user);
          await _appPreferences.saveUser(user);
          return Right(user);
        } else {
          return (Left(ErrorHandler.handle(response).failure));
        }
      } catch (error) {
        return (Left(ErrorHandler.handle(error).failure));
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, UserResponse>> updateUser(UserResponse user) async {
    if (await _networkInfo.isConnected) {
      try {
        final response = await _remoteDataSource.updateUser(user);
        if (response != null) {
          _appPreferences.saveUser(response);
          return Right(response);
        } else {
          return Left(DataSource.DEFAULT.getFailure());
        }
      } catch (error) {
        return (Left(ErrorHandler.handle(error).failure));
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, UserResponse>> getUser() async {
    if (await _networkInfo.isConnected) {
      try {
        UserResponse? user = await _appPreferences.getUser();
        final response = await _remoteDataSource.getUser(user?.uId ?? '');
        if (response != null) {
          _appPreferences.saveUser(response);
          return Right(response);
        } else {
          return Left(DataSource.DEFAULT.getFailure());
        }
      } catch (error) {
        return (Left(ErrorHandler.handle(error).failure));
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, void>> signOut() async {
    if (await _networkInfo.isConnected) {
      try {
        final response = await _remoteDataSource.signOut();
        return Right('');
      } catch (error) {
        return (Left(ErrorHandler.handle(error).failure));
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, void>> updateEmail(
      String newEmail, String password) async {
    if (await _networkInfo.isConnected) {
      try {
        UserResponse? user = await _appPreferences.getUser();
        final response = await _remoteDataSource.updateEmail(
            user!.email!, newEmail, password);
        if (response == 'success') {
          user.email = newEmail;
          final updatedResponse = await _remoteDataSource.saveUser(user);
          UserResponse? newUser = await _remoteDataSource.getUser(user.uId!);
          if (newUser != null) {
            await _appPreferences.saveUser(newUser);
          }
          return Right(response);
        } else {
          return (Left(ErrorHandler.handle(response).failure));
        }
      } catch (error) {
        return (Left(ErrorHandler.handle(error).failure));
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, void>> updatePassword(String newPassword) async {
    if (await _networkInfo.isConnected) {
      try {
        final response = await _remoteDataSource.updatePassword(newPassword);
        return Right(response);
      } catch (error) {
        return (Left(ErrorHandler.handle(error).failure));
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
