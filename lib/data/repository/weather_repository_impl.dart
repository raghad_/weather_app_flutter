import 'package:dartz/dartz.dart';
import 'package:weather_app/data/data_sources/weather_data_source.dart';
import 'package:weather_app/domain/model/weather_model.dart';
import 'package:weather_app/data/network/failure.dart';
import 'package:weather_app/domain/repository/weather_repository.dart';
import '../../app/app_prefs.dart';
import '../network/error_handler.dart';
import '../network/network_info.dart';

class WeatherRepositoryImpl extends WeatherRepository {
  final WeatherDataSource weatherDataSource;
  final NetworkInfo networkInfo;
  final AppPreferences appPreferences;

  WeatherRepositoryImpl(this.weatherDataSource, this.networkInfo,
      this.appPreferences,);

  @override
  Future<Either<Failure, Weather>> getWeatherByCityName(String cityName) async{
    if (await networkInfo.isConnected) {
      try {
        final response = await weatherDataSource.getWeatherByCityName(cityName);
          return Right(response);
      } catch (error) {
        return (Left(ErrorHandler.handle(error).failure));
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, Weather>> getWeatherByLocation(double lon, double lat) async{
    if (await networkInfo.isConnected) {
      try {
        final response = await weatherDataSource.getWeatherByLocation(lat, lon);
          return Right(response);
      } catch (error) {
        return (Left(ErrorHandler.handle(error).failure));
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
