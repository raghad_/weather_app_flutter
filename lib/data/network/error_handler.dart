import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../../presentation/resources/strings_manager.dart';
import 'failure.dart';

enum DataSource {
  SUCCESS,
  NO_CONTENT,
  BAD_REQUEST,
  FORBIDDEN,
  UNAUTHORISED,
  NOT_FOUND,
  INTERNAL_SERVER_ERROR,
  CONNECT_TIMEOUT,
  CANCEL,
  RECEIVE_TIMEOUT,
  SEND_TIMEOUT,
  CACHE_ERROR,
  NO_INTERNET_CONNECTION,
  DEFAULT,
  NOT_ACTIVE,
  TOO_MANY_REQUEST,
  UNPROCESSABLE_ENTITY,
  EMAIL_USED,
}

enum FirebaseSource { EMAIL_NOT_FOUND, WRONG_PASSWORD, EMAIL_EXIST }

class ErrorHandler implements Exception {
  late Failure failure;

  ErrorHandler.handle(dynamic error) {
    if (error is DioError) {
      // dio error so its error from response of the API
      print(error);
      failure = _handleError(error);
      failure.error = error;
    } else if (error is FirebaseAuthException) {
      failure = _handleFirebaseError(error);
    } else {
      // default error
      failure = DataSource.DEFAULT.getFailure();
    }
  }

  Failure _handleError(DioError error) {
    switch (error.type) {
      case DioErrorType.connectTimeout:
        return DataSource.CONNECT_TIMEOUT.getFailure();
      case DioErrorType.sendTimeout:
        return DataSource.SEND_TIMEOUT.getFailure();
      case DioErrorType.receiveTimeout:
        return DataSource.RECEIVE_TIMEOUT.getFailure();
      case DioErrorType.response:
        switch (error.response?.statusCode) {
          case ResponseCode.BAD_REQUEST:
            return DataSource.BAD_REQUEST.getFailure();
          case ResponseCode.FORBIDDEN:
            return DataSource.FORBIDDEN.getFailure();
          case ResponseCode.UNAUTHORISED:
            return DataSource.UNAUTHORISED.getFailure();
          case ResponseCode.NOT_FOUND:
            return DataSource.NOT_FOUND.getFailure();
          case ResponseCode.INTERNAL_SERVER_ERROR:
            return DataSource.INTERNAL_SERVER_ERROR.getFailure();
          case ResponseCode.NOT_ACTIVE:
            return DataSource.NOT_ACTIVE.getFailure();
          case ResponseCode.TOO_MANY_REQUEST:
            return DataSource.TOO_MANY_REQUEST.getFailure();
          case ResponseCode.UNPROCESSABLE_ENTITY:
            return DataSource.UNPROCESSABLE_ENTITY.getFailure();
          default:
            return DataSource.DEFAULT.getFailure();
        }
      case DioErrorType.cancel:
        return DataSource.CANCEL.getFailure();
      case DioErrorType.other:
        return DataSource.DEFAULT.getFailure();
    }
  }

  Failure _handleFirebaseError(FirebaseAuthException error) {
    switch (error.code) {
      case 'user-not-found':
        return FirebaseSource.EMAIL_NOT_FOUND.getFailure();
      case 'wrong-password':
        return FirebaseSource.WRONG_PASSWORD.getFailure();
      case 'email-already-in-use':
        return FirebaseSource.EMAIL_EXIST.getFailure();
      default:
        return DataSource.DEFAULT.getFailure();
    }
  }
}

extension DataSourceExtension on DataSource {
  Failure getFailure() {
    switch (this) {
      case DataSource.BAD_REQUEST:
        return Failure(ResponseCode.BAD_REQUEST, ResponseMessage.BAD_REQUEST);
      case DataSource.UNPROCESSABLE_ENTITY:
        return Failure(ResponseCode.UNPROCESSABLE_ENTITY,
            ResponseMessage.UNPROCESSABLE_ENTITY);
      case DataSource.FORBIDDEN:
        return Failure(ResponseCode.FORBIDDEN, ResponseMessage.FORBIDDEN);
      case DataSource.UNAUTHORISED:
        return Failure(ResponseCode.UNAUTHORISED, ResponseMessage.UNAUTHORISED);
      case DataSource.NOT_FOUND:
        return Failure(ResponseCode.NOT_FOUND, ResponseMessage.NOT_FOUND);
      case DataSource.INTERNAL_SERVER_ERROR:
        return Failure(ResponseCode.INTERNAL_SERVER_ERROR,
            ResponseMessage.INTERNAL_SERVER_ERROR);
      case DataSource.CONNECT_TIMEOUT:
        return Failure(
            ResponseCode.CONNECT_TIMEOUT, ResponseMessage.CONNECT_TIMEOUT);
      case DataSource.CANCEL:
        return Failure(ResponseCode.CANCEL, ResponseMessage.CANCEL);
      case DataSource.RECEIVE_TIMEOUT:
        return Failure(
            ResponseCode.RECEIVE_TIMEOUT, ResponseMessage.RECEIVE_TIMEOUT);
      case DataSource.SEND_TIMEOUT:
        return Failure(ResponseCode.SEND_TIMEOUT, ResponseMessage.SEND_TIMEOUT);
      case DataSource.CACHE_ERROR:
        return Failure(ResponseCode.CACHE_ERROR, ResponseMessage.CACHE_ERROR);
      case DataSource.NO_INTERNET_CONNECTION:
        return Failure(ResponseCode.NO_INTERNET_CONNECTION,
            ResponseMessage.NO_INTERNET_CONNECTION);
      case DataSource.DEFAULT:
        return Failure(ResponseCode.DEFAULT, ResponseMessage.DEFAULT);
      case DataSource.NOT_ACTIVE:
        return Failure(ResponseCode.NOT_ACTIVE, ResponseMessage.NOT_ACTIVE);
      case DataSource.TOO_MANY_REQUEST:
        return Failure(
            ResponseCode.TOO_MANY_REQUEST, ResponseMessage.TOO_MANY_REQUEST);
      default:
        return Failure(ResponseCode.DEFAULT, ResponseMessage.DEFAULT);
    }
  }
}

extension FirebaseDataSourceExption on FirebaseSource {
  Failure getFailure() {
    switch (this) {
      case FirebaseSource.WRONG_PASSWORD:
        return Failure(
            ResponseCode.WRONG_PASSWORD, ResponseMessage.WRONG_PASSWORD);
      case FirebaseSource.EMAIL_NOT_FOUND:
        return Failure(
            ResponseCode.EMAIL_NOT_FOUND, ResponseMessage.EMAIL_NOT_FOUND);
      case FirebaseSource.EMAIL_EXIST:
        return Failure(ResponseCode.EMAIL_EXIST, ResponseMessage.EMAIL_EXIST);
      default:
        return Failure(ResponseCode.DEFAULT, ResponseMessage.DEFAULT);
    }
  }
}

class ResponseCode {
  // API status codes
  static const int SUCCESS = 200; // success with data
  static const int NO_CONTENT = 201; // success with no content
  static const int BAD_REQUEST = 400; // failure, api rejected the request
  static const int FORBIDDEN = 403; // failure, api rejected the request
  static const int UNAUTHORISED = 401; // failure user is not authorised
  static const int TOO_MANY_REQUEST = 429;
  static const int NOT_FOUND =
      404; // failure, api url is not correct and not found
  static const int NOT_ACTIVE = 412;
  static const int UNPROCESSABLE_ENTITY = 422;
  static const int INTERNAL_SERVER_ERROR =
      500; // failure, crash happened in server side

  // local status code
  static const int DEFAULT = -1;
  static const int CONNECT_TIMEOUT = -2;
  static const int CANCEL = -3;
  static const int RECEIVE_TIMEOUT = -4;
  static const int SEND_TIMEOUT = -5;
  static const int CACHE_ERROR = -6;
  static const int NO_INTERNET_CONNECTION = -7;
  static const int EMAIL_NOT_FOUND = -8;
  static const int WRONG_PASSWORD = -9;
  static const int EMAIL_EXIST = -10;
}

class ResponseMessage {
  // API status codes
  static const String SUCCESS = AppStrings.msg_success; // success with data
  static const String NO_CONTENT =
      AppStrings.msg_no_content; // success with no content
  static const String BAD_REQUEST =
      AppStrings.msg_bad_request; // failure, api rejected the request
  static const String FORBIDDEN =
      AppStrings.msg_forbidden; // failure, api rejected the request
  static const String UNAUTHORISED =
      AppStrings.msg_unauthorised; // failure user is not authorised
  static const String NOT_FOUND =
      AppStrings.msg_not_found; // failure, api url is not correct and not found
  static const String INTERNAL_SERVER_ERROR = AppStrings.msg_something_wrong;
  static const String NOT_ACTIVE =
      AppStrings.msg_not_active; // failure, crash happened in server side
  static const String TOO_MANY_REQUEST = AppStrings.msg_too_many_req;
  static const String UNPROCESSABLE_ENTITY =
      AppStrings.msg_unprocessable_entity;

  // local status code
  static const String DEFAULT = AppStrings.msg_default;
  static const String CONNECT_TIMEOUT = AppStrings.msg_connection_out;
  static const String CANCEL = AppStrings.msg_cancel;
  static const String RECEIVE_TIMEOUT = AppStrings.msg_connection_out;
  static const String SEND_TIMEOUT = AppStrings.msg_connection_out;
  static const String CACHE_ERROR = AppStrings.msg_cache_error;
  static const String NO_INTERNET_CONNECTION = AppStrings.msg_not_internet;
  static const String EMAIL_NOT_FOUND = AppStrings.msg_email_not_found;
  static const String WRONG_PASSWORD = AppStrings.msg_wrong_password;
  static const String EMAIL_EXIST = AppStrings.msg_email_exist;
}

class ApiInternalStatus {
  static const int SUCCESS = 0;
  static const int FAILURE = 1;
}
