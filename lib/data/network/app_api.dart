import 'package:dio/dio.dart' hide Headers;
import 'package:retrofit/http.dart';

import '../../app/constant.dart';
import '../../domain/model/weather_model.dart';
//
part 'app_api.g.dart';
//
@RestApi(baseUrl: Constant.baseUrl)
abstract class AppServiceClient {
  factory AppServiceClient(Dio dio, {String baseUrl}) = _AppServiceClient;

  @GET("?lat={lat}&lon={lon}&appid=${Constant.apiKey}&lang=en&units=metric")
  Future<Weather> getWeatherByLocation(
      @Path() double lon,
      @Path() double lat);

  @GET("?q={cityName}&appid=${Constant.apiKey}&units=metric")
  Future<Weather> getWeatherByCityName(
      @Path() String cityName,
  );
}
