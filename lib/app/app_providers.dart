import 'package:provider/single_child_widget.dart';
import 'package:provider/provider.dart';
import 'package:weather_app/app/value_controller.dart';
List<SingleChildWidget> appProviders() {
  return <SingleChildWidget>[
    ChangeNotifierProvider<ValueController>(
      create: (context) => ValueController(),
    ),
  ];
}
