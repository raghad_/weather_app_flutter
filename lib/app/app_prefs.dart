import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import '../domain/model/user_model.dart';
import '../presentation/resources/language_manager.dart';

const String PREFS_KEY_LANG = "PREFS_KEY_LANG";
const String PREFS_KEY_USER = "PREFS_KEY_USER";

class AppPreferences {
  SharedPreferences _sharedPreferences;

  AppPreferences(this._sharedPreferences);

  Future<String> getAppLanguage() async {
    String? language = _sharedPreferences.getString(PREFS_KEY_LANG);

    if (language != null && language.isNotEmpty) {
      return language;
    } else {
      return LanguageType.ENGLISH.getValue();
    }
  }

  Future<void> setLanguageChanged() async {
    String currentLanguage = await getAppLanguage();
    if (currentLanguage == LanguageType.ARABIC.getValue()) {
      // save prefs with english lang
      _sharedPreferences.setString(
          PREFS_KEY_LANG, LanguageType.ENGLISH.getValue());
    } else {
      // save prefs with arabic lang
      _sharedPreferences.setString(
          PREFS_KEY_LANG, LanguageType.ARABIC.getValue());
    }
  }

  Future<Locale> getLocal() async {
    String currentLanguage = await getAppLanguage();
    if (currentLanguage == LanguageType.ARABIC.getValue()) {
      // return arabic local
      return ARABIC_LOCAL;
    } else {
      // return english local
      return ENGLISH_LOCAL;
    }
  }

  Future<void> saveUser(UserResponse user) async {
    await _sharedPreferences.setString(
        PREFS_KEY_USER, json.encode(user.toJson()));
  }

  Future<UserResponse?> getUser() async {
    final jsonUser = _sharedPreferences.getString(PREFS_KEY_USER);
    if (jsonUser != null) {
      return UserResponse.fromJson(json.decode(jsonUser));
    }
    return null;
  }

  Future<void> logout() async {
    String lang = await getAppLanguage();
    await _sharedPreferences.clear();
    String currentLang = await getAppLanguage();
    if (lang != currentLang) {
      await setLanguageChanged();
    }
  }
}
