import 'package:flutter/material.dart';

import '../../presentation/common_widgets/alert_dialog.dart';

abstract class BaseModel extends ChangeNotifier {
  bool _loading = false;

  bool get loading => _loading;
  bool _ready = false;

  bool get ready => _ready;
  late BuildContext context;

  /// faild in any api request
  bool _faild = false;

  bool get faild => _faild;

  void setFailed(bool isFaild) {
    if (_faild != isFaild) {
      _faild = isFaild;
      notifyListeners();
    }
  }

  void setLoading(bool isLoading) {
    if (_loading != isLoading) {
      _loading = isLoading;
      notifyListeners();
    }
  }

  void setReady(bool isReady) {
    if (_ready != isReady) {
      _ready = isReady;
      notifyListeners();
    }
  }

  // for multiple Api request if exist latter on
  Future fetchData(Iterable<Future> futures) async {
    setLoading(true);
    setFailed(false);

    await Future.wait(futures);
    setLoading(false);
  }
  showErrorDialog(String errorMessage, {VoidCallback? onPressed}) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return errorDialog(
              context: context, text: errorMessage, onPressed: onPressed);
        });
  }
}
