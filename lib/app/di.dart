import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:weather_app/domain/usecases/get_user_usecase.dart';
import 'package:weather_app/domain/usecases/get_weather_by_cityname.dart';
import 'package:weather_app/domain/usecases/get_weather_by_location.dart';
import 'package:weather_app/domain/usecases/register_usecase.dart';
import 'package:weather_app/domain/usecases/signout_usecase.dart';
import 'package:weather_app/domain/usecases/update_email_usecase.dart';
import 'package:weather_app/domain/usecases/update_password_usecase.dart';
import 'package:weather_app/domain/usecases/update_user_usecase.dart';
import 'package:weather_app/presentation/main/main_screen_model.dart';
import 'package:weather_app/presentation/profile/profile_screen_model.dart';
import 'package:weather_app/presentation/profile/update_email/update_email_screen_model.dart';
import '../data/data_sources/local_data_source.dart';
import '../data/data_sources/firebase_data_source.dart';
import '../data/data_sources/weather_data_source.dart';
import '../data/network/app_api.dart';
import '../data/network/dio_factory.dart';
import '../data/network/network_info.dart';
import '../data/repository/repository_impl.dart';
import '../data/repository/weather_repository_impl.dart';
import '../domain/repository/repository.dart';
import '../domain/repository/weather_repository.dart';
import '../domain/usecases/login_usecase.dart';
import '../presentation/auth/login/login_screen_model.dart';
import '../presentation/auth/register/register_screen_model.dart';
import 'app_prefs.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

final instance = GetIt.instance;
// dependency injection file

//to inject any module will be used during app lifecycle
Future<void> initAppModule() async {
  final sharedPrefs = await SharedPreferences.getInstance();

  //firebase
  final auth = FirebaseAuth.instance;
  final fireStore = FirebaseFirestore.instance;
  final FirebaseStorage firebaseStorage = FirebaseStorage.instance;
  instance.registerLazySingleton<SharedPreferences>(() => sharedPrefs);
  instance
      .registerLazySingleton<AppPreferences>(() => AppPreferences(instance()));
  instance.registerLazySingleton(() => auth);
  instance.registerLazySingleton(() => fireStore);
  instance.registerLazySingleton(() => firebaseStorage);

  // network info
  instance.registerLazySingleton<NetworkInfo>(
      () => NetworkInfoImpl(Connectivity()));

  //repository
  instance.registerLazySingleton<Repository>(
      () => RepositoryImpl(instance(), instance(), instance(), instance()));
  instance.registerLazySingleton<WeatherRepository>(() => WeatherRepositoryImpl(
        instance(),
        instance(),
        instance(),
      ));

  // dio factory
  instance.registerLazySingleton<DioFactory>(() => DioFactory());

  // app  service client
  final dio = await instance<DioFactory>().getDio();
  instance.registerLazySingleton<AppServiceClient>(() => AppServiceClient(dio));

  // remote data source
  instance.registerLazySingleton<FirebaseDataSource>(
      () => FirebaseDataSourceImplementer(instance(), instance()));

  instance.registerLazySingleton<WeatherDataSource>(
      () => WeatherDataSourceImplementer(
            instance(),
          ));
  // local data source
  instance.registerLazySingleton<LocalDataSource>(
      () => LocalDataSourceImplementer(instance()));
  initMainModule();
}

initLoginModule() {
  if (!GetIt.I.isRegistered<LoginUseCase>()) {
    instance.registerFactory<LoginUseCase>(() => LoginUseCase(instance()));
    instance.registerFactory<LoginScreenModel>(() => LoginScreenModel(
          instance(),
        ));
  }
}

initProfileModule() {
  if (!GetIt.I.isRegistered<GetUserUseCase>()) {
    instance.registerFactory<GetUserUseCase>(() => GetUserUseCase(instance()));
  }
  if (!GetIt.I.isRegistered<UpdateUserUseCase>()) {
    instance.registerFactory<UpdateUserUseCase>(() => UpdateUserUseCase(instance()));
  }
  if (!GetIt.I.isRegistered<UpdateEmailUseCase>()) {
    instance.registerFactory<UpdateEmailUseCase>(() => UpdateEmailUseCase(instance()));
  }
  if (!GetIt.I.isRegistered<UpdatePasswordUseCase>()) {
    instance.registerFactory<UpdatePasswordUseCase>(() => UpdatePasswordUseCase(instance()));
  }
  if (!GetIt.I.isRegistered<ProfileScreenModel>()) {
    instance.registerFactory<ProfileScreenModel>(() => ProfileScreenModel(instance(),instance(),instance(),instance()));
  }
}

initUpdateEmailModule(){
  if (!GetIt.I.isRegistered<UpdateEmailUseCase>()) {
    instance.registerFactory<UpdateEmailUseCase>(() => UpdateEmailUseCase(instance()));
  }
  if (!GetIt.I.isRegistered<UpdateEmailScreenModel>()) {
    instance.registerFactory<UpdateEmailScreenModel>(() => UpdateEmailScreenModel(instance()));
  }
}

initRegisterModule() {
  if (!GetIt.I.isRegistered<RegisterUseCase>()) {
    instance
        .registerFactory<RegisterUseCase>(() => RegisterUseCase(instance()));
    instance.registerFactory<RegisterScreenModel>(() => RegisterScreenModel(
          instance(),
        ));
  }
}

initMainModule() {
  if (!GetIt.I.isRegistered<GetWeatherByCityName>()) {
    instance.registerFactory<GetWeatherByCityName>(
        () => GetWeatherByCityName(instance()));
  }
  if (!GetIt.I.isRegistered<GetWeatherByLocation>()) {
    instance.registerFactory<GetWeatherByLocation>(
        () => GetWeatherByLocation(instance()));
  }
  if (!GetIt.I.isRegistered<SignOutUseCase>()) {
    instance.registerFactory<SignOutUseCase>(
            () => SignOutUseCase(instance()));
  }

  if (!GetIt.I.isRegistered<GetUserUseCase>()) {
    instance.registerFactory<GetUserUseCase>(() => GetUserUseCase(instance()));
  }
  if (!GetIt.I.isRegistered<MainScreenModel>()) {
    instance.registerFactory<MainScreenModel>(
        () => MainScreenModel(instance(), instance(),instance(),instance()));
  }
}

resetAppModule() async {
  await instance.reset(dispose: false);
  initAppModule();
}
